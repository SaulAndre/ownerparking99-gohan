import {
  faCoffee,
  faHome,
  faQrcode,
  faReceipt,
} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import React from 'react';
import {View, Text, StyleSheet, TouchableWithoutFeedback} from 'react-native';

export default function BottomNav({navigation}) {
  const handleClick = () => {};
  return (
    <View style={styles.bottomNavWrapper}>
      {/* <TouchableWithoutFeedback
        onPress={() => {
          navigation.navigate('Home');
        }}>
        <View style={styles.bottomNavItem}>
          <FontAwesomeIcon
            style={styles.bottomNavIcon}
            icon={faHome}
            size={20}
          />
        </View>
      </TouchableWithoutFeedback> */}
      <TouchableWithoutFeedback
        onPress={() => {
          navigation.navigate('Scan');
        }}>
        <View style={styles.bottomNavItem}>
          <FontAwesomeIcon
            style={styles.bottomNavIcon}
            icon={faQrcode}
            size={20}
          />
        </View>
      </TouchableWithoutFeedback>
      {/* <TouchableWithoutFeedback onPress={handleClick}>
        <View style={styles.bottomNavItem}>
          <FontAwesomeIcon
            style={styles.bottomNavIcon}
            icon={faReceipt}
            size={20}
          />
        </View>
      </TouchableWithoutFeedback> */}
    </View>
  );
}

const styles = StyleSheet.create({
  bottomNavWrapper: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    height: 50,
    borderTopWidth: 2,
    borderTopColor: '#eaeaea',
  },
  bottomNavItem: {
    flexGrow: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomNavIcon: {
    color: '#2196F3',
  },
});

import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default function CircleIcon({size, bgColor, icon, iconSize, iconColor}) {
  return (
    <View
      style={{
        width: size,
        height: size,
        backgroundColor: bgColor,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: size / 2,
      }}>
      <FontAwesomeIcon icon={icon} style={{color: iconColor}} size={iconSize} />
    </View>
  );
}

import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, ActivityIndicator, Button} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import moment from 'moment';
import {getParkiran, getTicket, getUser} from '../firebase/service';
import {useAuth} from '../firebase/auth';
import {currencyFormat} from '../utils';
import {timestamp} from '../firebase/config';

export default function DetailTicket({ticketId, borderColor, calculate}) {
  const [loading, setLoading] = useState(true);
  const [parkiran, setParkiran] = useState(null);
  const [ticket, setTicket] = useState(null);
  const [user, setUser] = useState(null);
  const [hours, setHours] = useState();
  const {currentUser} = useAuth();

  const fetchData = async () => {
    await getUser(currentUser.uid).then(res => {
      setUser(res);
      console.log('user-------------\n', res);
    });
    await getTicket(ticketId).then(res => {
      setTicket(res);
      calcHours(res.checkIn, res.checkOut);
      getParkiran(res.parkiranId).then(res2 => {
        setParkiran(res2);
        setLoading(false);
      });
    });
  };

  const calcHours = (date, checkout) => {
    if (checkout !== undefined) {
      setHours(Math.ceil((checkout - date) / 3600) - 1);
    } else {
      setHours(Math.ceil((timestamp.now() - date) / 3600) - 1);
    }
  };

  useEffect(() => {
    setLoading(true);
    fetchData();
  }, []);

  if (loading)
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#2196F3" />
      </View>
    );
  return (
    <View
      style={
        borderColor
          ? [styles.detailTicketWrapper, {borderColor: borderColor}]
          : [styles.detailTicketWrapper, {borderColor: '#2196F3'}]
      }>
      <View style={styles.qrWrapper}>
        <QRCode value={ticketId} size={300} />
      </View>
      <View style={styles.detailData}>
        <View style={{width: 300}}>
          <Text
            style={{
              color: 'black',
              fontSize: 18,
              fontWeight: 'bold',
              marginBottom: 10,
            }}>
            {parkiran.name}
          </Text>
          <Text style={{color: 'black'}}>
            Id Tiket:&nbsp;
            <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
              {ticket.id}
            </Text>
          </Text>
          <Text style={{color: 'black', fontSize: 14}}>
            Check-in:&nbsp;
            <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
              {moment(ticket.checkIn.toDate()).format('LLL')}
            </Text>
          </Text>
          <Text style={{color: 'black'}}>
            Nomor Kendaraan:&nbsp;
            <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
              {ticket.plateNumber}
            </Text>
          </Text>
        </View>
      </View>
      {calculate ? (
        <View style={styles.calculationWrapper}>
          <View style={styles.calculation}>
            <View style={styles.calculationItem}>
              <Text style={{color: 'black'}}>1 jam pertama</Text>
              <Text style={{color: '#2196F3', fontWeight: 'bold'}}>
                Rp{currencyFormat(parkiran.price)}
              </Text>
            </View>
            {ticket.checkOut ? null : null}
            {hours === 0 ? null : (
              <View style={styles.calculationItem}>
                <Text style={{color: 'black'}}>
                  {hours + ' jam berikutnya'}
                </Text>
                <Text style={{color: '#2196F3', fontWeight: 'bold'}}>
                  Rp{currencyFormat(1000 * hours)}
                </Text>
              </View>
            )}
          </View>
          <View style={styles.total}>
            <View
              style={{
                width: 300,
                display: 'flex',
                alignItems: 'flex-end',
                marginTop: 5,
              }}>
              <Text
                style={{
                  color: 'black',
                  fontSize: 24,
                  color: '#2196F3',
                  fontWeight: 'bold',
                }}>
                Rp{currencyFormat(parkiran.price + hours * 1000)}
              </Text>
            </View>
          </View>
        </View>
      ) : null}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 400,
  },
  detailTicketWrapper: {
    marginVertical: 15,
    padding: 20,
    display: 'flex',
    borderWidth: 3,
    borderRadius: 10,
    backgroundColor: 'white',
  },
  qrWrapper: {
    marginBottom: 20,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  detailData: {
    display: 'flex',
    alignItems: 'center',
  },
  calculationWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  calculation: {
    marginTop: 10,
    width: 300,
    paddingVertical: 10,
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderColor: '#eaeaea',
  },
  calculationItem: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
  },
  total: {},
});

import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {useAuth} from '../firebase/auth';
import {getUser, setCheckout} from '../firebase/service';

export default function Scan({navigation}) {
  const [parkiranId, setParkiranId] = useState('');
  const {currentUser} = useAuth();
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(null);

  const fetchData = async () => {
    await getUser(currentUser.uid).then(res => {
      setUser(res);
    });
    // getParkiran();
  };

  useEffect(() => {
    fetchData();
    console.log(currentUser.uid);
  }, []);

  const handleScan = async e => {
    navigation.navigate('Checkout', {ticketId: e.data});
  };

  const showAlert = () =>
    Alert.alert(
      'Error',
      'Something went wrong while creating your ticket. Please try again later',
      [
        {
          text: 'OK',
          onPress: () => {
            console.log('OK Pressed');
            navigation.navigate('Home');
          },
        },
      ],
    );

  return (
    <View style={styles.scanWrapper}>
      <Text>{parkiranId}</Text>
      <QRCodeScanner onRead={handleScan} />
      <Text style={{textAlign: 'center', color: 'white', fontSize: 14}}>
        Scan QR Code yang terdapat di tempat parkir tujuan
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  scanWrapper: {
    flex: 1,
    backgroundColor: '#414141',
  },
});

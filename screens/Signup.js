import React, {useState} from 'react';
import {TouchableOpacity, StyleSheet, View, Text, Button} from 'react-native';
import TextInput from '../components/TextInput';
import {emailValidator, fieldValidator, passwordValidator} from '../utils';
import {useAuth} from '../firebase/auth';
import KeyboardAvoidingView from 'react-native/Libraries/Components/Keyboard/KeyboardAvoidingView';

export default function Signup({navigation}) {
  const [email, setEmail] = useState({value: '', error: ''});
  const [password, setPassword] = useState({value: '', error: ''});
  const [name, setName] = useState({value: '', error: ''});
  const [address, setAddress] = useState({value: '', error: ''});
  const [note, setNote] = useState({value: '', error: ''});
  const [price, setPrice] = useState({value: '', error: ''});
  const [error, setError] = useState(null);
  const {signup, currentUser} = useAuth();

  const onSignupPressed = () => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    const nameError = fieldValidator(name.value);
    const addressError = fieldValidator(address.value);
    const noteError = fieldValidator(address.value);
    const priceError = fieldValidator(address.value);

    if (
      emailError ||
      passwordError ||
      nameError ||
      addressError ||
      noteError ||
      priceError
    ) {
      setEmail({...email, error: emailError});
      setPassword({...password, error: passwordError});
      setName({...name, error: nameError});
      setPlateNumber({...plateNumber, error: plateError});
      return;
    }
    try {
      console.log(
        email.value,
        password.value,
        name.value,
        address.value,
        note.value,
        price.value,
      );
      signup(
        email.value,
        password.value,
        name.value,
        address.value,
        note.value,
        price.value,
      ).then(res => {
        navigation.navigate('Home');
      });
    } catch (err) {
      console.log(err);
      if (err.code === 'auth/user-not-found') {
        setError("Couldn't find your account.");
      }
    }
  };

  if (currentUser) navigation.navigate('Home');
  return (
    <KeyboardAvoidingView style={styles.loginWrapper}>
      <View>
        <Text
          style={{
            fontSize: 18,
            color: 'white',
            fontWeight: 'bold',
            marginBottom: 10,
          }}>
          Please fill this form to start yours parking space!
        </Text>
        <Text>{error}</Text>
        <TextInput
          label="Email"
          returnKeyType="next"
          value={email.value}
          onChangeText={text => setEmail({value: text, error: ''})}
          error={!!email.error}
          errorText={email.error}
          autoCapitalize="none"
          autoCompleteType="email"
          textContentType="emailAddress"
          keyboardType="email-address"
          placeholder="Email"
          placeholderTextColor="#b7b7b7"
        />
        <TextInput
          label="Password"
          returnKeyType="done"
          value={password.value}
          onChangeText={text => setPassword({value: text, error: ''})}
          error={!!password.error}
          errorText={password.error}
          placeholder="Password"
          placeholderTextColor="#b7b7b7"
          secureTextEntry
        />
        <TextInput
          label="Parking Space Name"
          returnKeyType="done"
          value={name.value}
          onChangeText={text => setName({value: text, error: ''})}
          error={!!name.error}
          errorText={name.error}
          placeholder="Parking Space Name"
          placeholderTextColor="#b7b7b7"
        />
        <TextInput
          label="Address"
          returnKeyType="done"
          value={address.value}
          onChangeText={text => setAddress({value: text, error: ''})}
          error={!!address.error}
          errorText={address.error}
          placeholder="Street name and number"
          placeholderTextColor="#b7b7b7"
        />
        <TextInput
          label="Address Note"
          returnKeyType="done"
          value={note.value}
          onChangeText={text => setNote({value: text, error: ''})}
          error={!!note.error}
          errorText={note.error}
          placeholder="e.g. Gerbang Warna Biru"
          placeholderTextColor="#b7b7b7"
        />
        <TextInput
          label="Parking Price"
          returnKeyType="done"
          value={price.value}
          onChangeText={text => setPrice({value: text, error: ''})}
          error={!!price.error}
          errorText={price.error}
          placeholder="1000/2000/3000"
          placeholderTextColor="#b7b7b7"
          keyboardType={'numeric'}
        />
        <Button title="Signup" onPress={onSignupPressed} color="" />
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  loginWrapper: {
    flex: 1,
    backgroundColor: '#2196F3',
    padding: 20,
    justifyContent: 'center',
  },
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    color: 'white',
  },
  link: {
    fontWeight: 'bold',
    color: 'pink',
  },
});

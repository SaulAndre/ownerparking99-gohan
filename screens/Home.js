import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Button,
  StyleSheet,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import BottomNav from '../components/BottomNav';
import {ActivityIndicator} from 'react-native-paper';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useAuth} from '../firebase/auth';
import {
  getAllParkiranActiveTicket,
  getAllParkiranTicket,
  getNumbOfVehicleToday,
  getParkiran,
  getUser,
} from '../firebase/service';
import moment from 'moment';
import {threeDots} from '../utils';

export default function Home({navigation}) {
  const {currentUser, logout} = useAuth();
  const [parkiran, setParkiran] = useState();
  const [tickets, setTickets] = useState([]);
  const [activeTickets, setActiveTickets] = useState([]);
  const [todayVehicle, setVehicle] = useState();
  const [loading, setLoading] = useState(true);

  const removeActiveTicketFromTicketList = (listTickets, listActiveTickets) => {
    var intersect = [];
    if (listActiveTickets.length === 0) return listTickets;
    listTickets.forEach(ticket => {
      listActiveTickets.forEach(activeTicket => {
        console.log('ticket--------\n', ticket);
        console.log('active ticket--------\n', activeTicket);
        if (ticket.id !== activeTicket.ticketId) intersect.push(ticket);
      });
    });
    return intersect;
  };

  const fetchData = async () => {
    await getParkiran(currentUser.uid).then(res => {
      setParkiran(res);
    });
    await getAllParkiranTicket(currentUser.uid).then(res => {
      setTickets(res);
    });
    await getAllParkiranActiveTicket(currentUser.uid).then(res => {
      setActiveTickets(res);
    });
    setLoading(false);
    // await getNumbOfVehicleToday(currentUser.uid).then(res => {
    //   setVehicle(res);
    // });
  };

  const handleUserData = async userId => {
    var user;
    await getUser(userId).then(res => {
      user = res;
    });
    return user;
  };

  // useEffect(() => {
  //   console.log('useEffect----------\n', activeTickets);
  // }, [activeTickets]);
  useEffect(() => {
    if (tickets.length !== 0 && activeTickets !== 0) {
      setTickets(removeActiveTicketFromTicketList(tickets, activeTickets));
    }
  }, [activeTickets]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setLoading(true);
      fetchData();
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    fetchData();
  }, []);

  if (loading)
    <View style={styles.container}>
      <ActivityIndicator></ActivityIndicator>
    </View>;
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView style={styles.scrollView}>
        <View style={styles.header}>
          <Button
            title="logout"
            onPress={() => {
              logout().then(res => {
                navigation.navigate('Login');
              });
            }}
          />
          <Text
            style={{
              fontSize: 24,
              fontWeight: 'bold',
              color: 'white',
              marginBottom: 10,
            }}>
            {parkiran ? parkiran.name : null}
          </Text>
          <View style={styles.stats}>
            <View style={styles.statsItem}>
              <Text style={{fontSize: 16, marginBottom: 5}}>
                Kendaraan Parkir
              </Text>
              <Text
                style={{
                  fontSize: 24,
                  fontWeight: 'bold',
                  color: 'white',
                  textAlign: 'right',
                }}>
                {activeTickets.length}
              </Text>
            </View>
            <View style={styles.statsItem}>
              <Text style={{fontSize: 16, marginBottom: 5}}>
                Total Parkir Hari Ini
              </Text>
              <Text
                style={{
                  fontSize: 24,
                  fontWeight: 'bold',
                  color: 'white',
                  textAlign: 'right',
                }}>
                {todayVehicle}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.content}>
          <Text
            style={{
              fontSize: 18,
              color: 'black',
              fontWeight: 'bold',
              marginBottom: 10,
            }}>
            Kendaraan Parkir Saat Ini
          </Text>
          {activeTickets.length !== 0 ? (
            activeTickets.map(ticket => {
              return (
                <View style={styles.activeTicket} key={ticket.id}>
                  <View style={{display: 'flex', flexWrap: 'wrap'}}>
                    <Text style={{color: 'black'}}>
                      Id Tiket:&nbsp;
                      <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                        {threeDots(ticket.ticketId, 12)}
                      </Text>
                    </Text>
                    <Text style={{color: 'black', fontSize: 14}}>
                      Check-in:&nbsp;
                      <Text style={{fontWeight: 'bold', color: '#2196F3'}}>
                        {moment(ticket.checkIn.toDate()).format('lll')}
                      </Text>
                    </Text>
                  </View>
                  <View
                    style={{
                      width: 170,
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{fontSize: 14, color: 'black'}}>
                      Nomer Kendaraan
                    </Text>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        color: '#2196F3',
                        fontSize: 28,
                      }}>
                      {ticket.plateNumber}
                    </Text>
                  </View>
                </View>
              );
            })
          ) : (
            <View style={styles.activeTicket}>
              <Text style={{color: '#eaeaea'}}>
                Tidak ada kendaraan yang sedang parkir
              </Text>
            </View>
          )}
          <View style={styles.allTicket}>
            <Text
              style={{
                fontSize: 18,
                color: 'black',
                fontWeight: 'bold',
                marginBottom: 10,
              }}>
              History Parkir
            </Text>
            {tickets
              ? tickets.map(ticket => {
                  return (
                    <TouchableWithoutFeedback
                      key={ticket.id}
                      onPress={() =>
                        navigation.navigate('Checkout', {ticketId: ticket.id})
                      }>
                      <View style={styles.activeTicket} key={ticket.id}>
                        <View style={{display: 'flex', flexWrap: 'wrap'}}>
                          <Text style={{color: 'black'}}>
                            Id Tiket:&nbsp;
                            <Text
                              style={{fontWeight: 'bold', color: '#2196F3'}}>
                              {threeDots(ticket.id, 12)}
                            </Text>
                          </Text>
                          <Text style={{color: 'black', fontSize: 14}}>
                            Check-in:&nbsp;
                            <Text
                              style={{fontWeight: 'bold', color: '#2196F3'}}>
                              {moment(ticket.checkIn.toDate()).format('lll')}
                            </Text>
                          </Text>
                        </View>
                        <View
                          style={{
                            width: 170,
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <Text style={{fontSize: 14, color: 'black'}}>
                            Nomer Kendaraan
                          </Text>
                          <Text
                            style={{
                              fontWeight: 'bold',
                              color: '#2196F3',
                              fontSize: 28,
                            }}>
                            {ticket.plateNumber}
                          </Text>
                        </View>
                      </View>
                    </TouchableWithoutFeedback>
                  );
                })
              : null}
          </View>
        </View>
      </ScrollView>
      <BottomNav navigation={navigation} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    backgroundColor: '#2196F3',
    padding: 15,
    paddingVertical: 25,
    paddingTop: 15,
    borderBottomRightRadius: 20,
    borderBottomLeftRadius: 20,
  },
  stats: {
    display: 'flex',
    flexDirection: 'row',
  },
  statsItem: {
    marginRight: 10,
  },
  content: {
    padding: 20,
  },
  activeTicket: {
    marginBottom: 10,
    backgroundColor: 'white',
    padding: 15,
    borderRadius: 10,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  activeTicketDetail: {
    marginLeft: 15,
  },
});

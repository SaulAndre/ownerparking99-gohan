import React, {useEffect, useState} from 'react';
import {TouchableOpacity, StyleSheet, View, Text, Button} from 'react-native';
import TextInput from '../components/TextInput';
import {emailValidator, passwordValidator} from '../utils';
import {useAuth} from '../firebase/auth';
import CustomAlert from '../components/CustomAlert';

export default function Login({navigation}) {
  const [email, setEmail] = useState({value: '', error: ''});
  const [password, setPassword] = useState({value: '', error: ''});
  const {login, currentUser} = useAuth();
  const [errorMessage, setErrorMessage] = useState();

  useEffect(() => {
    if (currentUser) navigation.navigate('Home');
  });

  const onLoginPressed = () => {
    const emailError = emailValidator(email.value);
    const passwordError = passwordValidator(password.value);
    if (emailError || passwordError) {
      setEmail({...email, error: emailError});
      setPassword({...password, error: passwordError});
      return;
    }
    login(email.value, password.value)
      .then(res => {
        navigation.navigate('Home');
      })
      .catch(err => {
        console.log(err.code);
        if (err.code === 'auth/user-not-found') {
          setErrorMessage(
            'There is no user record corresponding to this identifier',
          );
        }
      });
  };

  return (
    <View style={styles.loginWrapper}>
      <Text
        style={{
          fontSize: 18,
          color: 'white',
          fontWeight: 'bold',
          marginBottom: 20,
        }}>
        Please login to start using parking99
      </Text>
      <TextInput
        label="Email"
        returnKeyType="next"
        value={email.value}
        onChangeText={text => setEmail({value: text, error: ''})}
        error={!!email.error}
        errorText={email.error}
        autoCapitalize="none"
        autoCompleteType="email"
        textContentType="emailAddress"
        keyboardType="email-address"
        placeholder="Email"
        placeholderTextColor="#b7b7b7"
      />
      <TextInput
        label="Password"
        returnKeyType="done"
        value={password.value}
        onChangeText={text => setPassword({value: text, error: ''})}
        error={!!password.error}
        errorText={password.error}
        placeholder="Password"
        placeholderTextColor="#b7b7b7"
        secureTextEntry
      />
      <View style={styles.forgotPassword}>
        {/* <TouchableOpacity
          onPress={() => navigation.navigate('ResetPasswordScreen')}>
          <Text style={styles.forgot}>Forgot your password?</Text>
        </TouchableOpacity> */}
      </View>
      {errorMessage ? (
        <View style={{marginBottom: 10}}>
          <CustomAlert title={errorMessage} type="danger" />
        </View>
      ) : null}
      <Button
        title="Login"
        mode="contained"
        onPress={onLoginPressed}
        color=""
      />
      <Text>{email.error}</Text>
      <View style={styles.row}>
        <Text>Don’t have an account? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('Signup')}>
          <Text style={styles.link}>Sign up</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  loginWrapper: {
    backgroundColor: '#2196F3',
    flex: 1,
    padding: 20,
    justifyContent: 'center',
  },
  forgotPassword: {
    width: '100%',
    alignItems: 'flex-end',
    marginBottom: 24,
  },
  row: {
    flexDirection: 'row',
    marginTop: 4,
  },
  forgot: {
    fontSize: 13,
    color: 'white',
  },
  link: {
    fontWeight: 'bold',
    color: 'pink',
  },
});

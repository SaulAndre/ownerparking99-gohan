const currencyFormat = x => {
  var parts = x.toString().split('.');
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  return parts.join(',');
};

const threeDots = (text, maxLength) => {
  if (text.length > maxLength) {
    return text.substring(0, maxLength) + '...';
  }
  return text;
};

const generateId = length => {
  var result = [];
  var characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result.push(
      characters.charAt(Math.floor(Math.random() * charactersLength)),
    );
  }
  return result.join('');
};

const emailValidator = email => {
  const re = /\S+@\S+\.\S+/;
  if (!email) return "Email can't be empty.";
  if (!re.test(email)) return 'Ooops! We need a valid email address.';
  return '';
};

const passwordValidator = password => {
  if (!password) return "Password can't be empty.";
  if (password.length < 5)
    return 'Password must be at least 5 characters long.';
  return '';
};

export function fieldValidator(name) {
  if (!name) return "This field can't be empty.";
  return '';
}

export {
  currencyFormat,
  threeDots,
  generateId,
  emailValidator,
  passwordValidator,
};

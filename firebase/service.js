import {db, timestamp} from './config';
import {
  collection,
  query,
  where,
  getDocs,
  setDoc,
  doc,
  deleteDoc,
} from 'firebase/firestore/lite';
import {generateId} from '../utils';

const getAllParkiranName = async () => {
  var data = [];
  const q = query(collection(db, 'parkiran'));
  const querySnapshot = await getDocs(q);
  querySnapshot.forEach(doc => {
    data.push({
      id: doc.data().id,
      name: doc.data().name,
    });
  });
  return data;
};

const getParkiran = async parkiranId => {
  var data;
  const q = query(collection(db, 'parkiran'), where('id', '==', parkiranId));
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => {
    data = doc.data();
    console.log('service', doc.data());
  });
  return data;
};

const getTicket = async ticketId => {
  var data;
  const q = query(collection(db, 'tiket'), where('id', '==', ticketId));
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => (data = doc.data()));
  return data;
};

const getAllParkiranTicket = async parkiranId => {
  var data = [];
  const q = query(
    collection(db, 'tiket'),
    where('parkiranId', '==', parkiranId),
  );
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => {
    console.log(doc.data());
    data.push(doc.data());
  });
  return data;
};

const getNumbOfVehicleToday = async parkingId => {
  // console.log('today------------\n', parkingId);
  var data = [];
  const startOfDay = new Date();
  startOfDay.setHours(0, 0, 0, 0);
  // console.log('date------------------\n', startOfDay);
  const q = query(
    collection(db, 'tiket'),
    where('parkingId', '==', parkingId),
    where('checkIn', '>', startOfDay),
  );
  const querySnapshot = await getDocs(q);
  querySnapshot.forEach(doc => {
    data.push(doc.data());
  });
  console.log('data------------------\n', data.length);
  return data.length;
};

const getAllParkiranActiveTicket = async parkiranId => {
  var data = [];
  const q = query(
    collection(db, 'activeTicket'),
    where('parkiranId', '==', parkiranId),
  );
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => data.push(doc.data()));
  return data;
};

const getUser = async userId => {
  var data;
  var q = query(collection(db, 'users'), where('id', '==', userId));
  var querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => {
    data = doc.data();
  });
  return data;
};

const setCheckout = async ticketId => {
  var activeTicketId;
  const q = query(
    collection(db, 'activeTicket'),
    where('ticketId', '==', ticketId),
  );
  const querySnapshot = await getDocs(q);
  querySnapshot.docs.map(doc => (activeTicketId = doc.data().id));
  await deleteDoc(doc(db, 'activeTicket', activeTicketId));

  await setDoc(
    doc(db, 'tiket', ticketId),
    {
      status: 'paid',
      checkOut: timestamp.now(),
    },
    {merge: true},
  ).catch(err => {
    console.log(err.code);
  });
};

export {
  getAllParkiranTicket,
  getAllParkiranName,
  getParkiran,
  getAllParkiranActiveTicket,
  getUser,
  getNumbOfVehicleToday,
  setCheckout,
  getTicket,
};

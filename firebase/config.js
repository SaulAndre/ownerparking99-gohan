import {initializeApp} from '@firebase/app';
import {getFirestore, Timestamp} from '@firebase/firestore/lite';
import firestore, {firebase} from '@react-native-firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyAmF6iRbMgFAVo8_GQiNNY5U-cYkJ1vHuE',
  authDomain: 'parking99-f42d4.firebaseapp.com',
  projectId: 'parking99-f42d4',
  storageBucket: 'parking99-f42d4.appspot.com',
  messagingSenderId: '1060878383795',
  appId: '1:1060878383795:web:e3bbb6116cfb08618c0ec6',
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// const storage = firebase.storage();
const db = getFirestore(app);

// const auth = firebase.auth();
const timestamp = Timestamp;

export {db, timestamp};
